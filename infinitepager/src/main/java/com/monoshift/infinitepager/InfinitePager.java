package com.monoshift.infinitepager;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.OverScroller;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * 스크롤 뷰의 로직을 흉내내서 만들어 본 뷰 페이저
 *
 * 특이사항 - 인덱스 단위가 아닌 오로지 모든 뷰의 이동을 InfinitePagerAdapter에 맞기는 페이징 클래스
 *
 * @author jaychoi
 *
 */
public class InfinitePager extends ViewGroup {

    private final FragmentRecycler mRecycle;
    private final int INVALID_POINTER = -1;
    // 무한 페이징 어댑터
    private InfinitePagerAdapter pagerAdapter = null;

    private FragmentManager mFragmentManager = null;
    /**
     * 스크롤 매니저
     */
    private OverScroller mScroller = null;
    private VelocityTracker mVelocityTracker = null;
    private PageMove mPageMove = PageMove.NONE;
    /**
     * 드래깅 상태를 보존
     */
    private boolean mIsBeingDragged = false;
    private int mTouchSlop = 1;
    private int mMaximumVelocity = 0;
    private int mMinimumVelocity = 0;
    private int mActivePointerId = 0;
    private float mLastMotionX = 0;
    private boolean scrollLock = false;

    private AsyncTask<Void, Void, Boolean> mScrollTask = null;

    public InfinitePager(Context context) {
        super(context);
        mRecycle = new FragmentRecycler();
        init(context);
    }

    public InfinitePager(Context context, AttributeSet attrs) {
        super(context, attrs);
        mRecycle = new FragmentRecycler();
        init(context);
    }

    public InfinitePager(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mRecycle = new FragmentRecycler();
        init(context);
    }

    private void init(Context context) {
        mScroller = new OverScroller(context);

        ViewConfiguration config = ViewConfiguration.get(context);
        this.mTouchSlop = config.getScaledTouchSlop();
        this.mMaximumVelocity = config.getScaledMaximumFlingVelocity();
        this.mMinimumVelocity = config.getScaledMinimumFlingVelocity();

        setOverScrollMode(OVER_SCROLL_NEVER);
    }

    public InfinitePagerAdapter getPagerAdapter() {
        return pagerAdapter;
    }

    public void setPagerAdapter(InfinitePagerAdapter pagerAdapter) {
        this.pagerAdapter = pagerAdapter;

        ViewConfiguration config = ViewConfiguration.get(getContext());
        this.mTouchSlop = config.getScaledTouchSlop();

        FragmentActivity fa = (FragmentActivity) getContext();
        this.mFragmentManager = fa.getFragmentManager();

        initPagingFragments();
    }

    protected void initPagingFragments() {
        mRecycle.beginTransaction();
        if (pagerAdapter.hasLeft() || pagerAdapter.hasRight()) {
            final LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (pagerAdapter.hasLeft()) {
                Fragment leftFragment = pagerAdapter.getFragment(inflater,
                        this, mRecycle.getFragment(Page.LEFT), Page.LEFT);
                mRecycle.putFragment(Page.LEFT, leftFragment);
            }
            if (pagerAdapter.hasRight()) {
                Fragment rightFragment = pagerAdapter.getFragment(inflater,
                        this, mRecycle.getFragment(Page.RIGHT), Page.RIGHT);
                mRecycle.putFragment(Page.RIGHT, rightFragment);
            }
            Fragment centerFragment = pagerAdapter.getFragment(inflater, this,
                    mRecycle.getFragment(Page.CENTER), Page.CENTER);
            mRecycle.putFragment(Page.CENTER, centerFragment);
        }
        mRecycle.commit();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int widthSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        int heightSpec = MeasureSpec.makeMeasureSpec(height,
                MeasureSpec.EXACTLY);
        for (int i = 0; i < getChildCount(); i++) {
            View view = getChildAt(i);
            measureChild(view, widthSpec, heightSpec);
        }

        setMeasuredDimension(width, height);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int width = getMeasuredWidth();
        final int height = getMeasuredHeight();

        for( Entry<Page, Fragment> entry : mRecycle.fragments()) {
            int left = 0;
            switch (entry.getKey()) {
                case LEFT:
                    left = -getWidth();
                    break;
                case CENTER:
                    break;
                case RIGHT:
                    left = getWidth();
                    break;
            }
            entry.getValue().getView().layout(left, 0, left + width, height);
        }
    }

    @Override
    public boolean shouldDelayChildPressedState() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        // TODO Auto-generated method stub
        return super.canScrollHorizontally(direction);
    }

//	@Override
//	public void computeScroll() {
//		// TODO Auto-generated method stub
//        if(scrollLock)
//            return;
//        boolean computed = mScroller.computeScrollOffset();
//        Log.i("computeScroll", ""+computed);
//		if (computed) {
//			int oldX = getScrollX();
//			int curX = mScroller.getCurrX();
//			if (oldX != curX) {
//				scrollTo(curX, 0);
//				postInvalidate();
//			}
//            else {
//                this.scrollLock = true;
//                new Handler().post(new Runnable() {
//                    @Override
//                    public void run() {
//                        mScroller.abortAnimation();
//                        mScroller.forceFinished(true);
//                        reallocatePages();
//                        scrollLock = false;
////                        scrollTo(0, 0);
//                    }
//                });
//            }
//		}
////        else {
////            new Handler().post(new Runnable() {
////                @Override
////                public void run() {
////                    mScroller.abortAnimation();
////                    mScroller.forceFinished(true);
////                    reallocatePages();
////                }
////            });
////        }
//	}

    /**
     * draw 메소드를 통해 UI Thread에서 재귀 형식으로 동작하는 빌어먹을 스크롤 이벤트가 짜증나서 별도로 떼내었는데 잘 될지 모르겠네요...
     * 잘 되면 좋겠음.
     */
    private void runScroll() {
        mScrollTask = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                if(mScroller.computeScrollOffset()) {
                    int oldX = getScrollX();
                    final int curX = mScroller.getCurrX();
                    if (oldX != curX) {
                        scrollTo(curX, 0);
                    }
                    return Boolean.TRUE;
                }
                return Boolean.FALSE;
            }

            @Override
            protected void onPostExecute(Boolean computed) {
                if(computed) {
                    runScroll();
                }
                else { // 여기서 스크롤이 종료되었음을 알 수 있다.
                    reallocatePages();
                }
            }
        };
        mScrollTask.execute();

    }

    private void reallocatePages() {
        if( 0 != getCurrentPage() ) {
            LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            pagerAdapter.onPaging(mPageMove);
            Fragment centerFragment = mRecycle.getFragment(Page.CENTER);
            Fragment rightFragment = mRecycle.getFragment(Page.RIGHT);
            Fragment leftFragment = mRecycle.getFragment(Page.LEFT);

            switch (mPageMove) {
                case TO_LEFT: {
                    mRecycle.putFragment(Page.CENTER, leftFragment);
                    mRecycle.putFragment(Page.RIGHT, centerFragment);
                    if(pagerAdapter.hasLeft()) {
                        mRecycle.putFragment(Page.LEFT, pagerAdapter.getFragment(li, this, rightFragment, Page.LEFT));
                    }
                    else {
                        mRecycle.putFragment(Page.LEFT, rightFragment);
                    }
                    break;
                }
                case TO_RIGHT: {
                    mRecycle.putFragment(Page.CENTER, rightFragment);
                    mRecycle.putFragment(Page.LEFT, centerFragment);
                    if(pagerAdapter.hasRight()) {
                        mRecycle.putFragment(Page.RIGHT, pagerAdapter.getFragment(li, this, leftFragment, Page.RIGHT));
                    }
                    else {
                        mRecycle.putFragment(Page.RIGHT, leftFragment);
                    }
                    break;
                }
            }

            mPageMove = PageMove.NONE;
            recycleVelocityTracker();
            requestLayout();
            scrollTo(0,0);
            postInvalidate();
        }
    }

    private int clamp(int x, int range) {
//		if (x < 0) {
//			return 0;
//		}
        return x;
    }

    private int getCurrentPage() {
        int page = 0 > getScrollX() ? ( getScrollX() - getWidth() / 2 ) / getWidth() : ( getScrollX() + getWidth() / 2 ) / getWidth();
        return page;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        final int action = ev.getAction();

        // 이동 액션이며, 현재 드래깅 도중인 경우 터치 이벤트를 인터셉트
        if (action == MotionEvent.ACTION_MOVE && mIsBeingDragged)
            return true;

        // 액션 마스크를 이용해 터치 액션만 처리할 수 있도록 필터링
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_MOVE: {

                // 터치 정보가 없을 경우 무시
                if (mActivePointerId == INVALID_POINTER)
                    break;

                final int pointerIndex = ev.findPointerIndex(mActivePointerId);
                final float x = ev.getX(pointerIndex);
                final int diffX = (int) Math.abs(x - mLastMotionX);
                if (mTouchSlop < diffX) {
                    mIsBeingDragged = true;
                    mLastMotionX = x;
                    initializeVelocityTracker();
                    mVelocityTracker.addMovement(ev);
                }
                break;
            }
            case MotionEvent.ACTION_DOWN: {
                final float x = ev.getX();
                final float y = ev.getY();
                if (false == inCenter(x, y)) {
                    mIsBeingDragged = false;
                    recycleVelocityTracker();
                }

                mLastMotionX = ev.getX();
                mActivePointerId = ev.getPointerId(0);

                initOrResetVelocityTracker();

                mVelocityTracker.addMovement(ev);

                mIsBeingDragged = false == mScroller.isFinished();

                break;
            }
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP: {
                mIsBeingDragged = false;
//			mActivePointerId = INVALID_POINTER;
//			recycleVelocityTracker();
//			if (mScroller.springBack(this.getScrollX(), this.getScrollY(), 0,
//					getWidth(), 0, 0)) {
//				invalidate();
//			}
                break;
            }
            case MotionEvent.ACTION_POINTER_UP: {

                onSecondPointerUp(ev);
                break;
            }
        }

        return mIsBeingDragged;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        initializeVelocityTracker();
        mVelocityTracker.addMovement(ev);

        final int action = ev.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                mIsBeingDragged = getChildCount() > 0;
                if (false == mIsBeingDragged)
                    return false;
                if (false == mScroller.isFinished()) {
                    mScroller.abortAnimation();
                }
                mLastMotionX = ev.getX();
                mActivePointerId = ev.getPointerId(0);
                break;
            }
            case MotionEvent.ACTION_MOVE: {

                if (mIsBeingDragged) {
                    // 현재 터치 인덱스를 습득
                    final int activePointerIndex = ev
                            .findPointerIndex(mActivePointerId);
                    // 터치 인덱스의 x 좌표를 습득
                    final float x = ev.getX(activePointerIndex);
                    // 기존 좌표와의 변이량을 기록
                    final int deltaX = (int) (mLastMotionX - x);
                    // 기존 좌표를 업데이트
                    mLastMotionX = x;
                    // 기존 스크롤 x 좌표를 기록
                    final int oldX = this.getScrollX();
                    // 페이징 너비를 기록
                    final int pageWidth = this.getWidth();
                    int scrollRange = 0;
                    /**
                     * 좌측으로 이동
                     */
                    if(0 > deltaX) {
                        if(pagerAdapter.hasLeft()) {
                            scrollRange = getWidth();
                        }
                        else {
                            scrollRange = 0;
                        }
                    }
                    /**
                     * 우측으로 이동
                     */
                    else if(0 < deltaX) {
                        if(pagerAdapter.hasRight()) {
                            scrollRange = getWidth() * 2;
                        }
                        else {
                            scrollRange = 0;
                        }
                    }
                    if (overScrollBy(deltaX, 0, oldX, 0, 0, 0, scrollRange, 0, true)) {
                        mVelocityTracker.clear();
                    }
                    onScrollChanged(getScrollX(), 0, oldX, 0);
                }
                break;
            }
            case MotionEvent.ACTION_UP: {
                if (mIsBeingDragged) {
                    final VelocityTracker vt = mVelocityTracker;
                    vt.computeCurrentVelocity(1000, mMaximumVelocity);
                    int initialVelocity = (int) vt.getXVelocity(mActivePointerId);
                    if (getChildCount() > 0) {
                        int page = getCurrentPage();
                        if (Math.abs(initialVelocity) > mMinimumVelocity) {
                            if(0 < initialVelocity) {
                                page--;
                                mPageMove = PageMove.TO_LEFT;
                            }
                            else if( 0 > initialVelocity) {
                                page++;
                                mPageMove = PageMove.TO_RIGHT;
                            }
                            else {
                                mPageMove = PageMove.NONE;
                            }
                        }

                        smoothScrollTo(page * getWidth(), 0, initialVelocity);
                        invalidate();
                        runScroll();
                    }
                    mActivePointerId = INVALID_POINTER;
                    mIsBeingDragged = false;
                    recycleVelocityTracker();
                }
                break;
            }
            case MotionEvent.ACTION_CANCEL: {
                if (mIsBeingDragged && getChildCount() > 0) {
                    if (mScroller.springBack(this.getScrollX(), this.getScrollY(),
                            0, 0, 0, getWidth())) {
                        invalidate();
                    }
                    mActivePointerId = INVALID_POINTER;
                    mIsBeingDragged = false;
                    recycleVelocityTracker();
                }
                break;
            }
            case MotionEvent.ACTION_POINTER_DOWN: {
                final int index = ev.getActionIndex();
                mLastMotionX = ev.getX(index);
                mActivePointerId = ev.getPointerId(index);
                break;
            }
            case MotionEvent.ACTION_POINTER_UP: {
                onSecondPointerUp(ev);
                mLastMotionX = ev.getX(ev.findPointerIndex(mActivePointerId));
                break;
            }
        }

        return true;
    }

    void smoothScrollTo(int x, int y, int velocity) {
        if (getChildCount() == 0) {
            // Nothing to do.
            return;
        }
        int sx = getScrollX();
        int sy = getScrollY();
        int dx = x - sx;
        int dy = y - sy;
        if (dx == 0 && dy == 0) {
            return;
        }

        float density = getContext().getResources().getDisplayMetrics().density;
        final float mBaseLineFlingVelocity = 2500.0f * density;
        final float mFlingVelocityInfluence = 0.4f;

        final float pageDelta = (float) Math.abs(dx) / getWidth();
        int duration = (int) (pageDelta * 100);

        velocity = Math.abs(velocity);
        if (velocity > 0)
            duration += (duration / (velocity / mBaseLineFlingVelocity)) * mFlingVelocityInfluence;
        else {
            duration += 100;
        }
        duration = Math.min(duration, 600);

        mScroller.startScroll(sx, sy, dx, dy, duration);
        invalidate();
    }

    @Override
    protected boolean overScrollBy(int deltaX, int deltaY,
                                   int scrollX, int scrollY,
                                   int scrollRangeX, int scrollRangeY,
                                   int maxOverScrollX, int maxOverScrollY,
                                   boolean isTouchEvent) {
        final int overScrollMode = OVER_SCROLL_ALWAYS;
        final boolean canScrollHorizontal =
                computeHorizontalScrollRange() > computeHorizontalScrollExtent();
        final boolean canScrollVertical =
                computeVerticalScrollRange() > computeVerticalScrollExtent();
        final boolean overScrollHorizontal = overScrollMode == OVER_SCROLL_ALWAYS ||
                (overScrollMode == OVER_SCROLL_IF_CONTENT_SCROLLS && canScrollHorizontal);
        final boolean overScrollVertical = overScrollMode == OVER_SCROLL_ALWAYS ||
                (overScrollMode == OVER_SCROLL_IF_CONTENT_SCROLLS && canScrollVertical);

        int newScrollX = scrollX + deltaX;
        if (!overScrollHorizontal) {
            maxOverScrollX = 0;
        }

        int newScrollY = scrollY + deltaY;
        if (!overScrollVertical) {
            maxOverScrollY = 0;
        }

        // Clamp values if at the limits and record
        final int left = -maxOverScrollX;
        final int right = maxOverScrollX + scrollRangeX;
        final int top = -maxOverScrollY;
        final int bottom = maxOverScrollY + scrollRangeY;

        boolean clampedX = false;
        if (newScrollX > right) {
            newScrollX = right;
            clampedX = true;
        } else if (newScrollX < left) {
            newScrollX = left;
            clampedX = true;
        }

        boolean clampedY = false;
        if (newScrollY > bottom) {
            newScrollY = bottom;
            clampedY = true;
        } else if (newScrollY < top) {
            newScrollY = top;
            clampedY = true;
        }

        onOverScrolled(newScrollX, newScrollY, clampedX, clampedY);

        return clampedX || clampedY;
    }

    /**
     * 하나 이상의 터치에서 하나의 터치가 사라졌을 경우 다른 터치의 포인터를 대신 사용
     *
     * @param ev
     */
    private void onSecondPointerUp(MotionEvent ev) {
        final int pointerIndex = (ev.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
        final int pointerId = ev.getPointerId(pointerIndex);
        if (mActivePointerId == pointerId) {
            // 안드로이드 만든 사람도 좀더 똑똑한 방식을 찾고 있음
            final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
            mLastMotionX = ev.getX(newPointerIndex);
            mActivePointerId = ev.getPointerId(newPointerIndex);
            if (null != mVelocityTracker) {
                mVelocityTracker.clear();
            }
        }
    }

    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX,
                                  boolean clampedY) {
        if (false == mScroller.isFinished()) {
            scrollTo(scrollX, 0);
            clearAnimation();
            if (clampedX)
                mScroller.springBack(scrollX, 0, 0, getWidth(), 0, 0);
        } else {
            super.scrollTo(scrollX, 0);
        }
    }

    @Override
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo info) {
        super.onInitializeAccessibilityNodeInfo(info);
        info.setScrollable(0 < getChildCount() && 0 < getWidth());
    }

    @Override
    public void onInitializeAccessibilityEvent(AccessibilityEvent event) {
        // TODO Auto-generated method stub
        super.onInitializeAccessibilityEvent(event);
        final boolean scrollable = 0 < getChildCount() && 0 < getWidth();
        event.setScrollable(scrollable);
        event.setScrollX(this.getScrollX());
        event.setScrollY(this.getScrollY());
    }

    private boolean inCenter(float x, float y) {
        return getLeft() < x && getRight() > x && getTop() < y
                && getBottom() > y;
    }

    private void initializeVelocityTracker() {
        if (null == mVelocityTracker) {
            mVelocityTracker = VelocityTracker.obtain();
        }
    }

    private void initOrResetVelocityTracker() {
        if (null == mVelocityTracker) {
            mVelocityTracker = VelocityTracker.obtain();
        } else {
            mVelocityTracker.clear();
        }
    }

    private void recycleVelocityTracker() {
        if (null == mVelocityTracker) {
            return;
        }
        mVelocityTracker.recycle();
        mVelocityTracker = null;
    }

    public enum PageMove {
        TO_LEFT("TO_LEFT"), TO_RIGHT("TO_RIGHT"), NONE("NONE");

        private final String tag;

        private PageMove(String tag) {
            this.tag = tag;
        }

        public String getTag() {
            return tag;
        }

        public String toString() {
            return "PageMove." + tag;
        }
    }

    public enum Page {
        LEFT("LEFT"), RIGHT("RIGHT"), CENTER("CENTER");

        private final String tag;

        private Page(String tag) {
            this.tag = tag;
        }

        public String getTag() {
            return tag;
        }

        public String toString() {
            return "Page." + tag;
        }
    }

    protected class FragmentRecycler {
        final HashMap<Page, Fragment> mCacheMap = new HashMap<Page, Fragment>(3);

        FragmentTransaction mFt = null;

        Set<Entry<Page, Fragment>> fragments() {
            return mCacheMap.entrySet();
        }

        void beginTransaction() {
            mFt = mFragmentManager.beginTransaction();
        }

        void add(Fragment fragment, Page page) {
            mFt.add(getId(), fragment, page.getTag());
        }

        void commit() {
            if (mFt.isEmpty())
                return;
            mFt.commit();
        }

        Fragment getFragment(Page page) {
            return mCacheMap.get(page);
        }

        void putFragment(Page page, Fragment fragment) {
            boolean needAddToContainer = false;
            if (null != mCacheMap.get(page)) {
                Fragment oldFragment = mCacheMap.get(page);
                if (oldFragment != fragment) {
                    mCacheMap.put(page, fragment);
                }
            } else {
                mCacheMap.put(page, fragment);
                needAddToContainer = true;
            }
            if (needAddToContainer) {
                mFt.add(getId(), fragment, page.getTag());
            }
        }
    }

}