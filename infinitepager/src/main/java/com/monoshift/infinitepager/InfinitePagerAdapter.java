package com.monoshift.infinitepager;

import android.app.Fragment;
import android.view.LayoutInflater;

import com.monoshift.infinitepager.InfinitePager.Page;
import com.monoshift.infinitepager.InfinitePager.PageMove;

public abstract class InfinitePagerAdapter {

	public abstract void onPaging(PageMove move);
	
	public abstract boolean hasLeft();
	public abstract boolean hasRight();
	
	public abstract Fragment getFragment(LayoutInflater inflater, InfinitePager parent, Fragment fragment, Page page);
	
}
