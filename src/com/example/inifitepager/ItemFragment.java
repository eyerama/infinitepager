package com.example.inifitepager;

import java.util.logging.Logger;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

public class ItemFragment extends Fragment {
		
		private String title = "title";
        private TextView mTitleView = null;
		
		private Logger logger;
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			
			logger = Logger.getLogger(this.getClass().getSimpleName());
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			logger.info("createView");
			FrameLayout layout = new FrameLayout(container.getContext());
			TextView textView = new TextView(container.getContext());
			FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			params.gravity = Gravity.CENTER;
			layout.addView(textView, params );
			textView.setBackgroundColor(Color.YELLOW);
			textView.setTextColor(Color.BLACK);
			textView.setId(100);
			textView.setGravity(Gravity.CENTER);
			layout.setBackgroundColor(Color.BLACK);
            mTitleView = textView;
			return layout;
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
			
			TextView textView = (TextView) getView().findViewById(100);
			textView.setText(title);
		}



		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
            if(null != mTitleView)
                mTitleView.setText(title);
		}

		
	}