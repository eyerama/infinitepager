package com.example.inifitepager;

import java.util.logging.Logger;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;

import com.example.inifitepager.InfinitePager.Page;
import com.example.inifitepager.InfinitePager.PageMove;

public class MainActivity extends FragmentActivity {

	private Logger logger;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.logger = Logger.getLogger(this.getClass().getSimpleName());
		setContentView(R.layout.activity_main);
		
		ToggleButton shuffle = (ToggleButton) findViewById(R.id.button_shuffle);
		ToggleButton repeat = (ToggleButton) findViewById(R.id.button_repeat);
		
		repeat.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				
			}
		});
		
		InfinitePager pager = (InfinitePager) findViewById(R.id.viewpager);
		InfinitePagerAdapter adapter = new InfinitePagerAdapter() {

			private int index = 0;
			
			@Override
			public void onPaging(PageMove move) {
				switch(move){
				case TO_LEFT:
					index --;
					break;
				case TO_RIGHT:
					index ++;
					break;
				default:
					break;
				}
			}

			@Override
			public Fragment getFragment(LayoutInflater inflater,
					InfinitePager parent, Fragment fragment, Page page) {
				ItemFragment fg = null;
				if(null == fragment) {
					fg = new ItemFragment();
					fragment = fg;
				}
                else {
                    fg = (ItemFragment)fragment;
                }
				final int i;
				switch(page){
				case LEFT:
					i = index-1;
					break;
				case RIGHT:
					i = index+1;
					break;
				default:
					i = index;
					break;
				}
                Log.i("getFragment", "Page:" + page.toString() + ", index:" + i);
                fg.setTitle(Integer.toString(i));
				return fragment;
			}

			@Override
			public boolean hasLeft() {
				if(-2 >= index)
					return false;
				return true;
			}

			@Override
			public boolean hasRight() {
				if(5 <= index)
					return false;
				return true;
			}
		};
		
		pager.setPagerAdapter(adapter);
	}
}

